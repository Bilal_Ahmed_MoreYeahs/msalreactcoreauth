﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactCoreWebAPIApplication.Model
{
    public class RootSiteModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string webUrl { get; set; }
        public string driveId { get; set; }
        public string drivePath { get; set; }
        public string folderPath { get; set; }
        public bool IsFolder { get; set; }
        public string Code { get; set; }
    }
}
