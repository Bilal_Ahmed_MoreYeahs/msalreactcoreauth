import React, { Component } from 'react';
import { msalAuth } from '../msal/MsalAuthProvider'
import axios from 'axios';
import configData from '../../src/config.json';

export class FetchData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            streamDocument: {},
            isLoading: true,
            error: null,
            accessToken: {},
            textValue: '',
            selectedCheckbox: []
        };  
    }

    downloadDocument() {
        const { selectedCheckbox } = this.state;

        for (var i = 0; i < selectedCheckbox.length; i++) {
            var drivePath1 = selectedCheckbox[i].DrivePath
            var name1 = selectedCheckbox[i].Name
            var code1 = selectedCheckbox[i].Code

            axios.get(configData.DownloadFile_URL,
                {
                    headers: {
                        token: this.state.accessToken.accessToken
                    }, params: {
                        code: code1,
                        filename: name1,
                        drivePath: drivePath1
                    }
                }).then((res) => {
                })
        }
        alert('file Downloaded Successfully')
        
    };

    getFolderDocuments(code, folderPath) {
        axios.get(configData.FolderDocument_URL,
            {
                headers: {
                    token: this.state.accessToken.accessToken
                }, params: {
                    siteId: code,
                    folderPath: folderPath
                }
            }).then((res) => {
                this.setState({
                    streamDocument: { value: res.data }, isLoading: false
                })
            })
            .catch((error) => {
                console.error(error)
            })
    };

    getSubSite() {
        if (this.state.textValue === "") {
            alert("Please enter a valid URL")
        } else {
            axios.get(configData.SubSite_URL, {
                headers: {
                    token: this.state.accessToken.accessToken
                }, params: {
                    relativeUrl: this.state.textValue,
                }
            }).then((res) => {
                this.setState({
                    streamDocument: { value: res.data }, isLoading: false
                })
            })
            .catch((error) => {
                console.error(error)
            })
        }
    };

    textChange(e) {
        this.setState({
            textValue: e.target.value
        })
    }

    handleChange(e, drivePath, name, code, i) {

        var markedCheckbox = document.getElementsByName('filename');  
        var vals = "";
        const values = [];
        for (var checkbox of markedCheckbox) {
            if (checkbox.checked) {
                vals += checkbox.value + ','
                this.state.streamDocument.value.map((res, i) => {
                    if (res.name === checkbox.value) {
                        var obj = {
                            DrivePath: res.drivePath,
                            Name: res.name,
                            Code: res.code
                        }                  
                        values.push(obj)
                    }
                }) 
            }
        }
        this.setState({
            selectedCheckbox: values
        })
        setTimeout(()=>{
        },[1000])
    }

    async componentDidMount() {
        try {
            const accessTokenRequest = {
                scopes: ["sites.read.all"]
            }
            var accessToken = null;
            try {
                accessToken = await msalAuth.acquireTokenSilent(accessTokenRequest);
            }
            catch (error) {
                accessToken = await msalAuth.acquireTokenPopup(accessTokenRequest);
            }

            this.setState({ accessToken })
        }
        catch (err) {
            var error = {};
            if (typeof (err) === 'string') {
                var errParts = err.split('|');
                error = errParts.length > 1 ?
                    { message: errParts[1], debug: errParts[0] } :
                    { message: err };
            } else {
                error = {
                    message: err.message,
                    debug: JSON.stringify(err)
                };
            }

            this.setState({
                streamDcument: {},
                isLoading: false,
                error: error
            });
        }

        const params = {
            token: this.state.accessToken.accessToken,
            path: 'https://graph.microsoft.com/v1.0/'
        };

        axios.get(configData.SiteRoot_URL, { headers: params }).then((res) => {
            this.setState({
                streamDocument: res.data, isLoading: false
            })
        })
        .catch((error) => {
            console.error(error)
        })        
    }

    render() {
        let contents = this.state.isLoading
            ? <p><em>Loading...</em></p>
            : <div><pre>{JSON.stringify(this.state.streamDocument, null, 2)}</pre></div>;

        let contents1 = [];
        if (this.state.streamDocument.value != null) {
            contents1 = this.state.streamDocument.value.map((res, i) => {
                let CheckName = `filename`
                if (res.isFolder === true) {
                    return (
                        <div className="row">
                            <div className="col-md-3">
                                <div onClick={() => this.getFolderDocuments(res.code, res.folderPath)}><img src="https://image.freepik.com/free-vector/illustration-data-folder-icon_53876-6329.jpg" width="50px" height="50px" alt="Folder Image" />
                                    {res.name}
                                </div>
                            </div>
                        </div>
                    )
                } else {
                    return (
                        <div className="row">
                            <div className="col-md-3">
                                <div>
                                    <input type="checkbox" value={res.name} name={CheckName} onChange={e => this.handleChange(e, res.drivePath, res.name, res.code, i)} />
                                    {res.name}
                                </div>
                            </div>
                        </div>
                    )
                }
            })
        }

        return (
            <div>
                <h1 id="tabelLabel">Document info</h1>
                <p>This information comes from graph.</p>

                <div className="row col-md-8">
                    <input type="text" name="subSiteUrl" id="subSiteUrl" value={this.state.textValue} onChange={(e) => this.textChange(e)} />
                    <input type="button" name="Submit" value="Submit Url" onClick={() => this.getSubSite()} />

                    <input type="button" name="Submit" value="Download Selected Files" onClick={() => this.downloadDocument()} />
                </div>

                {contents1}
            </div>
        );
    }
}