﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Identity.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ReactCoreWebAPIApplication.Helpers;
using ReactCoreWebAPIApplication.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace ReactCoreWebAPIApplication.Controllers
{
    [Route("api/home")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _env;
        private readonly IGraphSDKHelper _graphSdkHelper;

        public HomeController(IConfiguration configuration, IHostingEnvironment hostingEnvironment, IGraphSDKHelper graphSdkHelper)
        {
            _configuration = configuration;
            _env = hostingEnvironment;
            _graphSdkHelper = graphSdkHelper;
        }

        //SharePoint Functions
        //To get the Root of the SharePoint document folder
        [Route("GetSiteRoot")]
        [HttpGet]
        public async Task<IActionResult> GetSiteRoot()
        {
            try
            {
                //Getting Token and Path from frontend
                var token = Request.Headers["token"];
                string path = "/sites/root/lists/Documents";

                //Authenticating the token for response
                var response = await CallGraphApiOnBehalfOfUser(token, path);

                //Retrieving Site code
                var result = JObject.Parse(Convert.ToString(response));

                //Fetching SiteId
                string siteId = result["parentReference"]["siteId"].ToString();
                string[] array = siteId.Split(",");
                string sitecode = array[array.Length - 2];

                //Passing site code to get list of files and folders
                var siteDocumentList = await GetSiteDocuments(sitecode);
                return Ok(siteDocumentList);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //Get Files and Folders present in the root of the default sharepoint site
        [Route("GetSiteDocuments")]
        [HttpGet]
        public async Task<IActionResult> GetSiteDocuments(string code)
        {
            try
            {
                var token = Request.Headers["token"];
                string path = "/sites/" + code + "/lists/Documents/drive/root/children";
                var response = await CallGraphApiOnBehalfOfUser(token, path);
                var result = JObject.Parse(Convert.ToString(response));

                List<RootSiteModel> lstDocuments = new List<RootSiteModel>();
                foreach (JObject child in result["value"])
                {
                    RootSiteModel model = new RootSiteModel();
                    model.id = child["id"].ToString();
                    model.name = child["name"].ToString();
                    model.webUrl = child["webUrl"].ToString();
                    model.driveId = child["parentReference"]["driveId"].ToString();
                    model.drivePath = child["parentReference"]["path"].ToString();
                    model.folderPath = child["parentReference"]["path"].ToString() + "/" + child["name"].ToString();
                    model.IsFolder = child["folder"] != null ? true : false;
                    model.Code = code;
                    lstDocuments.Add(model);
                }

                return Ok(lstDocuments);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //This method is created to get the files and sub folders present inside a folder
        //This method is dynamically taking the path and fetching the response
        //Before fetching it is authenticating the request
        [Route("GetFolderDocuments")]
        [HttpGet]
        public async Task<IActionResult> GetFolderDocuments(string siteId, string folderPath)
        {
            try
            {
                string token = Request.Headers["token"];
                string path = "";
                
                if (folderPath.Contains("drives"))
                {
                    path = "sites/" + siteId + "/" + folderPath + ":/children";
                }
                else
                {
                    path = "sites/" + siteId + "/lists/" + folderPath + "/drive/root/children";
                }

                var response = await CallGraphApiOnBehalfOfUser(token, path);
                var result = JObject.Parse(Convert.ToString(response));

                List<RootSiteModel> lstDocuments = new List<RootSiteModel>();
                foreach (JObject child in result["value"])
                {
                    RootSiteModel model = new RootSiteModel();
                    model.id = child["id"].ToString();
                    model.name = child["name"].ToString();
                    model.webUrl = child["webUrl"].ToString();
                    model.driveId = child["parentReference"]["driveId"].ToString();
                    model.drivePath = child["parentReference"]["path"].ToString();
                    model.folderPath = child["parentReference"]["path"].ToString() + "/" + child["name"].ToString();
                    model.IsFolder = child["folder"] != null ? true : false;
                    model.Code = siteId;
                    lstDocuments.Add(model);
                }

                return Ok(lstDocuments);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //This method is created for SubSite, Child & Default site
        //Once User will paste the path of the SubSite, Child Site
        //This method will first fetch the Site Id then it will Encode the URL 
        //After that with Encoded URl it will fetch the site details and get the child drive items
        //After that Setting the response using loop
        [Route("GetSubSite")]
        [HttpGet]
        public async Task<IActionResult> GetSubSite(string relativeUrl)
        {
            #region Get document library list using site url
            try
            {
                string token = Request.Headers["token"];
                string path = "sites/root/lists";

                if (relativeUrl.Contains("_layouts"))
                {

                    var baseUrl = relativeUrl.Split("_layouts", StringSplitOptions.None);
                    var siteurl = baseUrl[0].ToString().Split("https://", StringSplitOptions.None)[1].ToString();
                    if (siteurl.Contains("sites"))
                    {
                        var siteurlarray = siteurl.Split("sites", StringSplitOptions.None);
                        path = "sites/" + siteurlarray[0].ToString().Replace("/", "") + ":/sites/" + siteurlarray[1].ToString().Replace("/", "");
                    }
                }

                //Authenticating the token for response
                var response = await CallGraphApiOnBehalfOfUser(token, path);
                var result = JObject.Parse(Convert.ToString(response));

                //Fetching the site id
                string siteCode = "";

                if (path.Contains("sites/root/lists"))
                {
                    foreach (JObject child in result["value"])
                    {
                        siteCode = child["parentReference"]["siteId"].ToString();
                    }
                }
                else
                {
                    siteCode = result["id"];
                }

                if (relativeUrl.Contains("_layouts"))
                {
                    var drivePath = "sites/" + siteCode + "/drives";
                    var driveResponse = await CallGraphApiOnBehalfOfUser(token, drivePath);
                    var driveResult = JObject.Parse(Convert.ToString(driveResponse));

                    List<RootSiteModel> lstDocuments = new List<RootSiteModel>();
                    foreach (JObject child in driveResult["value"])
                    {
                        RootSiteModel model = new RootSiteModel();
                        model.name = child["name"].ToString();
                        model.folderPath = child["name"].ToString();
                        model.driveId = child["driveType"].ToString();
                        model.IsFolder = model.driveId == "documentLibrary" ? true : false;
                        model.Code = siteCode;
                        lstDocuments.Add(model);
                    }
                    return Ok(lstDocuments);

                    #region Certificate Code
                    //var baseUrl = relativeUrl.Split("_layouts", StringSplitOptions.None);
                    //var siteurl = baseUrl[0].ToString();

                    ////Added a self-signed certificate which is required to authenticate
                    //string currentDirectory = Directory.GetCurrentDirectory();
                    //string filePath = System.IO.Path.Combine(currentDirectory, "Certificate", "test.pfx");
                    //X509Certificate2 certificate = new X509Certificate2(filePath, "test@123", X509KeyStorageFlags.MachineKeySet);

                    //string tenantId = _configuration.GetValue<string>("AzureAd:TenantId");
                    //string applicationId = _configuration.GetValue<string>("AzureAd:ClientId");
                    //IConfidentialClientApplication confApp = ConfidentialClientApplicationBuilder.Create(applicationId)
                    //.WithAuthority($"https://login.microsoftonline.com/{tenantId}")
                    //.WithCertificate(certificate)
                    //.Build();

                    //string[] array = siteCode.Split(",");
                    //string url = array[array.Length - 3];

                    //string sharePointUrl = "https://" + url;
                    //var scopes = new[] { $"{sharePointUrl}/.default" };

                    ////Acquiring new access token on behalf of a user and certificate
                    //var authenticationResult = await confApp.AcquireTokenForClient(scopes).ExecuteAsync();
                    //string newToken = authenticationResult.AccessToken;

                    //using (var client = new HttpClient())
                    //{
                    //    client.BaseAddress = new Uri(siteurl);
                    //    client.DefaultRequestHeaders.Accept.Clear();
                    //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", newToken);

                    //    //Rest api called to get the 101 status type in the sharepoint library which is Document Library
                    //    HttpResponseMessage restResponse = await client.GetAsync("_api/web/lists?$select=Id,Title,ServerRelativeUrl&$filter=BaseTemplate%20eq%20101%20and%20hidden%20eq%20false&$expand=RootFolder");
                    //    if (restResponse.IsSuccessStatusCode)
                    //    {
                    //        var trialData = await restResponse.Content.ReadAsStringAsync();
                    //        var restResult = JObject.Parse(Convert.ToString(trialData));

                    //        List<RootSiteModel> lstDocuments = new List<RootSiteModel>();
                    //        foreach (JObject child in restResult["value"])
                    //        {
                    //            RootSiteModel model = new RootSiteModel();
                    //            model.name = child["Title"].ToString();
                    //            model.folderPath = child["Title"].ToString();
                    //            model.IsFolder = child["RootFolder"] != null ? true : false;
                    //            model.Code = siteCode;
                    //            lstDocuments.Add(model);
                    //        }
                    //        return Ok(lstDocuments);
                    //    }
                    //}
                    #endregion
                }
                else
                {
                    string base64Value = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(relativeUrl));
                    string encodedUrl = "u!" + base64Value.TrimEnd('=').Replace('/', '_').Replace('+', '-');

                    string newEncodedUrl = "/shares/" + encodedUrl + "/driveItem";
                    var encodedResponse = await CallGraphApiOnBehalfOfUser(token, newEncodedUrl);

                    try
                    {
                        var newResult = JObject.Parse(Convert.ToString(encodedResponse));
                        string drivePath = "";
                        string newPath = "";
                        string name = newResult["name"].ToString();

                        //When we are passing the root url it doesn't get the Path
                        //So we are checking for null reference and changing the path accordingly
                        if (newResult["parentReference"]["path"] != null)
                        {
                            drivePath = newResult["parentReference"]["path"].ToString();
                            newPath = "sites/" + siteCode + "/" + drivePath + "/" + name + ":/children";
                        }
                        else
                        {
                            drivePath = newResult["parentReference"]["driveId"].ToString();
                            newPath = "sites/" + siteCode + "/" + "drives/" + drivePath + "/" + name + "/children";
                        }

                        var newResponse = await CallGraphApiOnBehalfOfUser(token, newPath);
                        var finalResult = JObject.Parse(Convert.ToString(newResponse));

                        List<RootSiteModel> lstDocuments = new List<RootSiteModel>();
                        foreach (JObject child in finalResult["value"])
                        {
                            RootSiteModel model = new RootSiteModel();
                            model.id = child["id"].ToString();
                            model.name = child["name"].ToString();
                            model.webUrl = child["webUrl"].ToString();
                            model.driveId = child["parentReference"]["driveId"].ToString();
                            model.drivePath = child["parentReference"]["path"].ToString();
                            model.folderPath = child["parentReference"]["path"].ToString() + "/" + child["name"].ToString();
                            model.IsFolder = child["folder"] != null ? true : false;
                            model.Code = siteCode;
                            lstDocuments.Add(model);
                        }

                        return Ok(lstDocuments);
                    }
                    catch (Exception ex)
                    {
                        return BadRequest(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();

            #endregion
        }

        //With this method we are able to easily download the files
        //It creates the download URl and using web client it downloads it
        [Route("StreamFile")]
        [HttpGet]
        public async Task<IActionResult> StreamFile(string code, string drivePath, string filename)
        {
            try
            {
                var token = Request.Headers["token"];
                var path = "sites/" + code + drivePath + "/" + filename;
                var response = await CallGraphApiOnBehalfOfUser(token, path);

                var result = JObject.Parse(Convert.ToString(response));
                var downloadUrl = result["@microsoft.graph.downloadUrl"].ToString();
                var downloadFileName = result["name"].ToString();

                using (WebClient webClient = new WebClient())
                {
                    webClient.DownloadFile(downloadUrl, "D:/SharePointDocs/" + downloadFileName);
                }

                string message = "Your file has been downloaded";
                return Ok(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //Generic method to Authorize the token
        //Also used to get the response from graph api using the path provided
        private static async Task<dynamic> CallGraphApiOnBehalfOfUser(string accessToken, string path)
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var response = await client.GetAsync("https://graph.microsoft.com/v1.0/" + path);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    dynamic result = JsonConvert.DeserializeObject(content);
                    return result;
                }
                else
                {
                    throw new HttpRequestException($"Invalid status code in the HttpResponseMessage: {response.StatusCode}.");
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}
