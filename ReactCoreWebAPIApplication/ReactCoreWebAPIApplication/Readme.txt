﻿To run the project please try to follow the steps mentioned below

1. First Clone the project from the repo https://bitbucket.org/Bilal_Ahmed_MoreYeahs/msalreactcoreauth/src/master/
2. Once you are done open your project
3. Please remove the .vs folder present in the solution
4. Try to Clean and Rebuild your solution first
5. Once done, please start the project by selecting "IIS Express" and your project should start
6. After that open a command prompt as administrator and go to your ClientApp folder and open it in the command prompt
7. Next run the "NPM INSTALL" command
8. After npm install you need to run NPM START in your application
9. Now within the browser you need to enter your credentials like from any of the given below

1st Instance
	username: - doe@testsharepointAuth.onmicrosoft.com
	password: - test@123

10. For SharePoint address, Login to portal.office.com with any of the Username and Password and with the App Launcher at the top left select SharePoint.
11. There you can find the Default, Child or Subsite present.
12. Select any of your site and proceed to Document section for further testing.

Note: - 

1. Before testing locally please create a folder in drive D:/SharePointDocs, and then try to download the files selected
2. There is a config.json file inside the src folder of client app which is having the urls defined for the API's. Please check those.
